# Summary

* [前言](README.md)
* [基础篇](js_intro.md)
  * [1.数据类型](js_part1_dataType.md)
  * [2.运算符](js_part1_calc_operator.md)
  * [3.操作符](js_part1_operator.md)
  * [4.数组](js_part1_array.md)
  * [5.对象](js_part1_object.md)
  * [6.属性描述符](property-descriptor.md)
  * [7.函数](js_part1_function.md)
  * [8.this对象](js_part1_this.md)
  * [9.原型](js_part1_prototype.md)
* [进阶篇](js_part2.md)
  * [词法作用域](js_part2_scope.md)
  * [变量和变量赋值](js_part2_varaible_scope.md)
  * [变量提升](js-part2-variable.md)
  * [模块](js_part2_module.md)
  * [类：面向对象编程](js_part2_OO.md)
  * [垃圾收集](js_part2_garage_collection.md)
  * [js内存泄漏](js_part2_memory_leak.md)
  * [柯里化](js_part2_curry.md)
  * [event loop](js_part2_event-loop.md)
  * [作用域](js_part2_scope.md)
    * [作用域链](js_part2_scope/scope-chain.md)
    * [改变作用域链](js_part2_scope/change-scope-chain.md)
    * [动态作用域](js_part2_scope/dong-tai-zuo-yong-yu.md)
  * [闭包](js_part2_closure.md)
  * [Ajax](js_part2_ajax.md)
  * [正则表达式](regex.md)
    * [规则](regex/regex-rule.md)
    * [组匹配](regex/regex-group-match.md)
  * [HTTP encoding](js_part2-http-encoding.md)
* [DOM编程](js-dom.md)
  * [HTML集合](js-dom-html-collection.md)
  * [遍历DOM](js-dom-traverse.md)
  * [重排和重绘](js-dom-layout-repain.md)
* [ES6](js_es6.md)
  * [数组](js_es6_array.md)
  * [String](js_es6_string.md)
  * [函数](js_es6_function.md)
  * [解构赋值](js_es6_assignment.md)
  * [非标准库对象](js_es6_non_standard_obj.md)
  * [Symbols](js_es6_symbols.md)
  * [模块 Modules](modules.md)
* [设计模式](js_design.md)
  * [工厂模式](js-design-factory.md)
  * [迭代器模式](js-design-iterator.md)
  * [装饰器模式](js-design-decorator.md)
  * [策略模式](js-design-strategy.md)
  * [外观模式](js-design-appearance.md)
  * [代理模式](js-design-pattern.md)
  * [中介者模式](js-pattern-mediator.md)
  * [观察者模式](js-design-observer.md)
* [其他](js-others.md)
  * [客户端存储](js-other-storage.md)
  * [HTML5-History API](js-other-history-api.md)
  * [前端的各种转义](js-other-encoding.md)
  * [Cookie](js-other-cookie.md)
  * [UI设计模式](js-other-ui设计模式.md)
  * [window.performance](js-other-window.performance.md)
    * [performance.timing](js-other-window.performance/performance.timing.md)
    * [performance.memory](js-other-window.performance/performance.memory.md)
    * [performance.getEntries](js-other-window.performance/performance.getentries.md)
    * [performance.now](window.performance/performance.now.md)
  * [ESLint](js-other-eslint.md)
* [Tips](js_part3.md)
  * [为什么用事件委托](js_part3_event_delegate.md)
  * [document.write](js_part3_documentwrite.md)
  * [Number范围](js_part3_number范围.md)
  * [心得](js_part3_summary.md)
  * [练习题](js_part3_practise.md)

